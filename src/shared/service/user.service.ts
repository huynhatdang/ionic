import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { api } from '../constant';
import { Storage } from '@ionic/storage';

@Injectable()
export class UserAPI {

    constructor(private _http: HttpClient, private _storage: Storage) { }

    createUser(user) : Observable<any>{
        console.log('davo');
        return this._http.post(api+'auth/signup',user)
        .catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    login(user) : Observable<any>{
        this._storage.remove('user');
        this._storage.remove('token');
        return this._http.post(api+'auth/signin',user)
        .catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    // getWork(): Observable<IWork[]> {
    //     return this._http.get(this.baseUrl)
    //         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    // }
}