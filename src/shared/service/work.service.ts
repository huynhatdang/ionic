import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { IWork } from './../interface/work'
import { api } from '../constant';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';


@Injectable()
export class WorkAPI {

    headers: HttpHeaders;
    constructor(private _http: HttpClient, private _storage: Storage) {
        this._storage.get('token').then((token) => {
            this.headers = new HttpHeaders().set('x-access-token', token);
            console.log(this.headers);
        })
    }

    getWork(): Observable<IWork[]> {
        return this._http.get(api + 'work')
            .catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    searchWork(searchText: string, filter: any): Observable<any> {
        console.log(this.headers);
        return this._http.get(api + 'work/search?searchText=' + searchText + '&searchBy=' + filter, { headers: this.headers })
            .catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    createWork(work): Observable<any> {
        return this._http.post(api + 'work/create', work, { headers: this.headers })
            ._catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    addFavorite(workId): Observable<any> {
        return this._http.put(api + 'work/addFavorite/' + workId,'' ,{ headers: this.headers })
        ._catch((error: any) => Observable.throw(error.error || 'Server error'));

    }

    getFavorite(): Observable<any> {
        console.log(this.headers)
        return this._http.get(api + 'work/getFavorite', {headers: this.headers})
            .catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    sendCV(workId): Observable<any>{
        return this._http.put(api+ 'work/sendCV/'+ workId,'',{headers: this.headers})
        ._catch((error: any) => Observable.throw(error.error || 'Server error'));
    }
}