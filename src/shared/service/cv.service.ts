import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
// import { IWork } from './../interface/work';
import { api } from '../constant';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';


@Injectable()
export class CvAPI {

    headers: HttpHeaders;
    constructor(private _http: HttpClient, private _storage: Storage) { 
        this._storage.get('token').then((token) => {
            this.headers = new HttpHeaders().set('x-access-token', token);
            console.log(this.headers);
        })
    }

    createCV(cv) : Observable<any[]>{
        return this._http.post(api+'cv/create',cv, {headers: this.headers})
        .catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    getCV(cvId): Observable<any>{
        return this._http.get(api+'cv/get/'+cvId,{headers: this.headers})
        .catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    checkCV(): Observable<any>{
        return this._http.get(api+'cv/checkCV',{headers: this.headers})
        .catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

}