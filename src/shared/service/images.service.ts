import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import 'rxjs/add/operator/map';

/*
  Generated class for the ImagesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ImagesProvider {


    apiURL = 'http://192.168.0.110:8001/api/company/upload';
    constructor(public http: HttpClient, private transfer: FileTransfer) {
        console.log('Hello ImagesProvider Provider');
    }

    uploadImage(img) {
        let url = this.apiURL;

        // File for Upload
        var targetPath = img;

        var options: FileUploadOptions = {
            fileKey: 'image',
            chunkedMode: false,
            fileName: targetPath,
            mimeType: 'multipart/form-data',
            headers: { 'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTBkNDVlY2ZiOThlNjIxZTBkYjYxYmEiLCJlbWFpbCI6IkphbmV0b25ld0BnbWFpbC5jb20iLCJ1c2VybmFtZSI6IkphbmV0b25ldyIsIm5hbWUiOiJKYW5ldG9uZXciLCJyb2xlcyI6ImNvbXBhbnkiLCJfX3YiOjAsImlhdCI6MTUxMDkwNTMwOX0.lM2aZ_Iq3kOfIATmz2YsCrI2H7vVZ-FGSJCRnY7dMk4'} 
        };

        const fileTransfer: FileTransferObject = this.transfer.create();

        // Use the FileTransfer to upload the image
        return fileTransfer.upload(targetPath, url, options);
    }
}
