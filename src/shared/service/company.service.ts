import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { api } from '../constant';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage'; 


@Injectable()
export class CompanyAPI {
    headers: HttpHeaders;
    constructor(private _http: HttpClient, private _storage: Storage) {
        this._storage.get('token').then((token)=>{
            this.headers = new HttpHeaders().set('x-access-token', token);
        })
     }

    createCompany(company): Observable<any>{
        return this._http.post(api+'company/create',company,{headers: this.headers}).map((data)=>{
            console.log(data);
        })
                ._catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    uploadImage(path): Observable<any>{
        return this._http.post(api+'company/create',path,{headers: this.headers})
                ._catch((error: any) => Observable.throw(error.error || 'Server error'));
    }

    // getWork(): Observable<IWork[]> {
    //     return this._http.get(api+'work')
    //         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    // }
}