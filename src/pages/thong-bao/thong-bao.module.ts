import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ThongBaoPage } from './thong-bao';

@NgModule({
  declarations: [
    ThongBaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ThongBaoPage),
  ],
})
export class ThongBaoPageModule {}
