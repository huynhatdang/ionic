import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DashboardPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {

  timViecRoot = 'TimViecPage';
  viecCuaToiRoot = 'ViecCuaToiPage';
  settingRoot = 'SettingPage';

  createWorkRoot = 'CreateWorkPage';
  workPostedRoot = 'WorkPostedPage';
  // settingCompanyRoot = ''
  // taocvRoot = 'RegisterInfoPage'
  // thongBaoRoot = 'ThongBaoPage'
  // khacRoot = 'KhacPage'

  role: string;


  constructor(public navCtrl: NavController, public navParams: NavParams) { 
    this.role = this.navParams.get('role');
  }

}
