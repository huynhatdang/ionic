import { UserAPI } from './../../shared/service/user.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { role } from '../../shared/constant';

/**
 * Generated class for the RegisterCompanyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-company',
  templateUrl: 'register-company.html',
  providers: [FormBuilder]
})
export class RegisterCompanyPage {

  registerForm : FormGroup;
  submited: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private _userAPI: UserAPI, private toastCtrl: ToastController) {
    this.registerForm = this.formBuilder.group({
      fullname: ['', Validators.required],
      username: ['',Validators.compose([Validators.required,Validators.maxLength(15)])],
      email: ['',Validators.compose([Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)])],      
      password: ['',Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterCompanyPage');
  }
  addShake(type:string){
    document.querySelector(`.${type}`).classList.add("error","animated","shake");
    setTimeout(()=> {
    document.querySelector(`.${type}`).classList.remove("error","animated","shake");
    }, 1000);
  }

  register(){
    if( this.registerForm.valid){
      let user = {
        email: this.registerForm.get('email').value,
        username: this.registerForm.get('username').value,
        fullname: this.registerForm.get('fullname').value,
        password: this.registerForm.get('password').value,
        roles: role.COMPANY
      }
      this._userAPI.createUser(user).subscribe((data)=>{
        let toast = this.toastCtrl.create({
					message: 'Bạn đã tạo tài khoản thành công!',
					duration: 1000,
					position: 'bottom'
				  });
				  toast.present();
				  this.navCtrl.push('LoginPage', {}, { animate: true, animation: 'ios-transition' })
      }, (err)=>{
        let toast = this.toastCtrl.create({
          message: 'Tạo tài khoản không thành công!',
          duration: 1000,
					position: 'bottom'
				  });
				  toast.present();
      })
    }
    let arr =['fullname','username','email','password'];
    arr.forEach((e)=>{
      if(this.registerForm.get(e).invalid){
        this.addShake(e);
      }
    })   
  }
}
