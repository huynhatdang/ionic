import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { ViewChild } from '@angular/core';

/**
 * Generated class for the SliderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-slider',
  templateUrl: 'slider.html',
})
export class SliderPage {
  splash = true;
  tabBarElement : any;
  @ViewChild(Slides) slides: Slides;
  skipMsg: string = "Skip";

  constructor(public navCtrl: NavController) {
    this.tabBarElement = document.querySelector('.tabbar');
    console.log(this.tabBarElement);
  }

  ionViewDidLoad() {
    setTimeout(() => this.splash = false, 6000);
  }
  skip() {
    this.navCtrl.push('LoginPage');
  }

  slideChanged() {
    if (this.slides.isEnd())
      this.skipMsg = "Alright, I got it";
  }


}
