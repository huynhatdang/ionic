import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { UserAPI } from '../../shared/service/user.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { role } from '../../shared/constant';
import { Storage } from '@ionic/storage';



/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [FormBuilder]
})
export class LoginPage {

  loginForm: FormGroup;

  user = { "username": "", "password": "" };
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, private _userAPI: UserAPI, public formBuilder: FormBuilder, private _stoarage: Storage) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.maxLength(15)])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  addShake(type: string) {
    document.querySelector(`.${type}`).classList.add("error", "animated", "shake");
    setTimeout(() => {
      document.querySelector(`.${type}`).classList.remove("error", "animated", "shake");
    }, 1000);
  }

  login() {

    if (this.loginForm.valid) {
      let user = {
        username: this.loginForm.get('username').value,
        password: this.loginForm.get('password').value
      }
      this._stoarage.clear().then(() => {
        this._stoarage.get('user').then((data) => { console.log(data) });
        this._userAPI.login(user).subscribe((data) => {
          this._stoarage.set('user', data.user);
          this._stoarage.set('token', data.token);
          let toast = this.toastCtrl.create({
            message: 'Bạn đã đăng nhập thành công!',
            duration: 1000,
            position: 'bottom'
          });
          toast.present();
          if (data.user.roles === role.USER) {
            this.navCtrl.push('DashboardPage', {role: 'user'}, { animate: true, animation: 'ios-transition' })
          } else if (data.user.roles === role.COMPANY) {
            console.log(data.user.company)
            if (data.user.company) {
              this.navCtrl.push('DashboardPage', {role: 'company'}, { animate: true, animation: 'ios-transition' })
            } else {
              this.navCtrl.push('CompanyInfoPage',{} , { animate: true, animation: 'ios-transition' })

            }
          }
        }, (err) => {
          let toast = this.toastCtrl.create({
            message: 'Đăng Nhập thất bại!',
            duration: 1000,
            position: 'bottom'
          });
          toast.present();
        })
      })
    }
    let arr = ['username', 'password'];
    arr.forEach((e) => {
      if (this.loginForm.get(e).invalid) {
        this.addShake(e);
      }
    })

  }

  register() {
    this.navCtrl.push('RegisterPage', {}, { animate: true, animation: 'ios-transition', duration: 1000, easing: 'ease-in-out' });
  }

}
