import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import { RegisterUserPage } from '../register-user/register-user';
import { RegisterCompanyPage } from '../register-company/register-company';

@NgModule({
  declarations: [
    RegisterPage,
    RegisterUserPage,
    RegisterCompanyPage
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
  ],
})
export class RegisterPageModule {}
