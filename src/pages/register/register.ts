import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  show: string = 'user';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    document.querySelector('.user').classList.add("active");
  }

  registerCompany(){
    document.querySelector('.company').classList.add("active");
    document.querySelector('.user').classList.remove("active");
    this.show = 'company';
  }

  registerUser(){
    document.querySelector('.user').classList.add("active");
    document.querySelector('.company').classList.remove ("active");
    this.show = 'user';
  }
}
