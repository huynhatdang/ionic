import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CvAPI } from './../../shared/service/cv.service';

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private _cvAPI: CvAPI) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }
  ionViewWillEnter(){
      this._cvAPI.checkCV().subscribe((data)=>{
        console.log(data);
        this.navCtrl.push('ProfileUserPage',{cvId: data.cv._id});
      },(err)=>{
        console.log(err);
      })
  }

  createCV(){
    this.navCtrl.push('RegisterInfoPage',{}, { animate: true, animation: 'ios-transition' })
  }
}
