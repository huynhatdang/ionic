import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';


declare var google: any;
/**
 * Generated class for the TestmapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-testmap',
  templateUrl: 'testmap.html',
})
export class TestmapPage {

  location: any = {
    lat: null,
    lng: null,
    name: null,
  };
  map: any;
  workList: any[];
  @ViewChild('map') mapElement;
  @ViewChild("places")
  public places: ElementRef;

  constructor(public navCtrl: NavController, public param: NavParams, public platform: Platform, private geolocation: Geolocation, private viewCtrl: ViewController) {
    /*platform.ready().then(() => {
     this.InitMap();
    });*/
    this.workList = param.get('listWork');
  }
  ionViewDidLoad() {
    this.initAutocomplete();
    if (this.workList) {
      this.getMarkers();
    }
  }



  initAutocomplete() {
    if (this.workList) {
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: this.workList[0].company.address.location.latitude, lng: this.workList[0].company.address.location.longitude },
        zoom: 10,
        mapTypeId: 'roadmap'
      });
    } else {
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 10.8600654, lng: 106.7112863 },
        zoom: 10,
        mapTypeId: 'roadmap'
      });
    }

    // Create the search box and link it to the UI element.
    let input = document.getElementById('pac-input');
    let searchBox = new google.maps.places.SearchBox(input);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    // Bias the SearchBox results towards current map's viewport.
    this.map.addListener('bounds_changed', () => {
      searchBox.setBounds(this.map.getBounds());
    });

    let markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', () => {
      let places = searchBox.getPlaces();
      if (places.length == 0) {
        return;
      }
      // Clear out the old markers.
      markers.forEach((marker) => {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      const bounds = new google.maps.LatLngBounds();
      places.forEach((place) => {
        console.log(place.formatted_address);
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        this.location.lat = place.geometry.location.lat();
        this.location.lng = place.geometry.location.lng();
        this.location.name = place.formatted_address;

        const icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: this.map,
          icon: icon,
          title: place.name,
          animation: google.maps.Animation.DROP,
          position: place.geometry.location
        }));
        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      this.map.fitBounds(bounds);
    });
  }
  getMarkers() {
    this.workList.forEach((work) => this.addMarkersToMap(work));
    // for (let _i = 0; _i < this.workList.length; _i++) {
    //    this.addMarkersToMap(this.workList[_i].company.address.location);
    // }
  }

  last() {
    this.viewCtrl.dismiss(this.location);
  }

  keyup() {
    console.log(123);
  }

  addMarkersToMap(work) {
    var position = new google.maps.LatLng(work.company.address.location.latitude, work.company.address.location.longitude);
    var workMarker = new google.maps.Marker({ position: position, title: work.name });
    this.addInfoWindow(workMarker, work);
    workMarker.setMap(this.map);
  }

  addInfoWindow(marker, work) {
    const templateWindow = '<div id="iw-container">' +
      '<div class="iw-title">' + work.name + '</div>' +
      '<div class="iw-content">' +
      '<div class="iw-subTitle">' + work.company.address.address + '</div>' +
      '<p>' + work.salary.salaryFrom + '-' + work.salary.salaryTo + work.salary.type + '</p>' +
      '<img src="http://maps.marnoto.com/en/5wayscustomizeinfowindow/images/vistalegre.jpg" alt="Porcelain Factory of Vista Alegre" height="115" width="83">' +
      '<p>' + work.company.description + '</p>' +
      '<div class="iw-subTitle">Contacts</div>' +
      '<p>' + work.company.address.address + '<br>' +
      '<br>Phone.' + work.company.contact + '<br>e-mail: huycongxe@gmail.com<br>website:' + work.company.website + '</p>' +
      '</div>' +
      '<div class="iw-bottom-gradient"></div>' +
      '</div>';
    let infoWindow = new google.maps.InfoWindow({
      content: templateWindow
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }


}
