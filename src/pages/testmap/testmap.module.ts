import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestmapPage } from './testmap';

@NgModule({
  declarations: [
    TestmapPage,
  ],
  imports: [
    IonicPageModule.forChild(TestmapPage),
  ],
})
export class TestmapPageModule {}
