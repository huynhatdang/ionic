import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { WorkAPI } from '../../shared/service/work.service';

/**
 * Generated class for the CreateWorkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-work',
  templateUrl: 'create-work.html',
})
export class CreateWorkPage {

  workForm: FormGroup;
  workForm2: FormGroup;
  @ViewChild('createWorkSlider') createWorkSlider: any;

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, private toastCtrl: ToastController, private _workAPI: WorkAPI) {
    this.workForm = formBuilder.group({
      name: ['', Validators.required],
      position: ['', Validators.required],
      major: ['', Validators.required],
      dateTime: ['']
    });
    // this._storage.get('user').then((data)=>console.log(data));
    // this._storage.get('token').then((data)=>console.log(data));
    this.workForm2 = formBuilder.group({
      requirement: ['', Validators.required],
      salaryFrom: ['', Validators.required],
      salaryTo: ['', Validators.required],
      type: ['', Validators.required],
      benifit: ['', Validators.required]
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateWorkPage');
  }
  next() {
    this.createWorkSlider.slideNext();
  }

  save() {
    let work = {
      name: this.workForm.get("name").value,
      position: this.workForm.get("position").value,
      major: this.workForm.get("major").value,
      dateTime: this.workForm.get("dateTime").value,
      requirement: this.workForm2.get("requirement").value,
      salary:{
        salaryFrom: this.workForm2.get("salaryFrom").value,
        salaryTo: this.workForm2.get("salaryTo").value,
        type: this.workForm2.get("type").value
      },
      benifit: this.workForm2.get("benifit").value
    }
    this._workAPI.createWork(work).subscribe((data) => {
      let toast = this.toastCtrl.create({
        message: 'Thanh cong!',
        duration: 1000,
        position: 'bottom'
      });
      toast.present();
      // this.navCtrl.push('CreateWorkPage');
    }, (err) => {
      // this.navCtrl.push('CreateWorkPage');
      console.log(err);
    });
  }

}
