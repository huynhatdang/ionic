// import { UsernameValidator } from './../../../validators/username';
// import { AgeValidator } from './../../../validators/age';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Moment from 'moment';
import { CvAPI } from '../../../shared/service/cv.service';


/**
 * Generated class for the RegisterInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-register-info',
	templateUrl: 'register-info.html',
})
export class RegisterInfoPage {

	@ViewChild('signupSlider') signupSlider: any;

	slideOneForm: FormGroup;
	slideTwoForm: FormGroup;
	slideThreeForm: FormGroup;
	slideFourForm: FormGroup;
	english: string;
	errorMessage: string;

	privacies: string[] = ['Trung học', 'Trung cấp', 'Cao đẳng', 'Cử nhân', 'Thạc sĩ', 'Tiến sĩ'];
	startDate: String = new Date().toISOString();
	submitAttempt: boolean = false;
	constructor(public navCtrl: NavController, public formBuilder: FormBuilder, private _cvApi: CvAPI, private toastCtrl: ToastController) {

		this.slideOneForm = formBuilder.group({
			phoneNumber: ['', Validators.compose([Validators.maxLength(12), Validators.minLength(10), Validators.pattern('[0-9]+'), Validators.required])],
			experience: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
			major: ['', Validators.compose([Validators.maxLength(30), Validators.required])]
		});

		this.slideTwoForm = formBuilder.group({
			university: ['', Validators.compose([Validators.required])],
			privacy: ['', Validators.required],
			endTime: ['', Validators.required],
			startTime: ['', Validators.required],
		});

		this.slideThreeForm = formBuilder.group({
			position: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*')])],
			company: ['', Validators.required],
			endTime: [''],
			startTime: ['', Validators.required],
			description: ['']
		});

	}

	ionViewDidLoad() {
	}

	next() {
		this.signupSlider.slideNext();
	}

	prev() {
		this.signupSlider.slidePrev();
	}

	save() {

		this.submitAttempt = true;

		if (!this.slideOneForm.valid) {
			this.signupSlider.slideTo(0);
		}
		else if (!this.slideTwoForm.valid) {
			this.signupSlider.slideTo(1);
		}
		else {
			let cv = {
				certificate: [{
					university: this.slideTwoForm.value.university,
					major: this.slideTwoForm.value.major,
					certificate: this.slideTwoForm.value.privacy,
					from: this.slideTwoForm.value.startTime,
					to: this.slideTwoForm.value.endTime
				}],
				experience: [{
					company: this.slideThreeForm.value.company,
					position: this.slideThreeForm.value.position,
					from: Moment(this.slideThreeForm.value.startTime).format('YYYY-MM'),
					to: Moment(this.slideThreeForm.value.startTime).format('YYYY-MM') || ''
				}],
				language: [{
					language: 'English',
					typeL: this.english
				}]
			}
			this._cvApi.createCV(cv).subscribe(cv => {
				let toast = this.toastCtrl.create({
					message: 'Bạn đã tạo CV thành công!',
					duration: 1000,
					position: 'middle'
				  });
				  toast.present();
				  this.navCtrl.push('DashboardPage', {}, { animate: true, animation: 'ios-transition' })
			},()=>{
				let toast = this.toastCtrl.create({
					message: 'Tạo CV không thành công!',
					duration: 1000,
					position: 'middle'
				  });
				  toast.present();
			});
			console.log("success!");
		}

	}

}
