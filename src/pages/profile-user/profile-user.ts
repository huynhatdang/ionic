import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CvAPI } from '../../shared/service/cv.service';

/**
 * Generated class for the ProfileUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-user',
  templateUrl: 'profile-user.html',
})
export class ProfileUserPage {

  cvID: string;
  cv: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private _cvAPI: CvAPI) {
    this.cvID = this.navParams.get('cvId');
  }

  ionViewDidLoad() {
    this._cvAPI.getCV(this.cvID).subscribe((data)=>{
      console.log(data);
      this.cv = data;
    })
    console.log('ionViewDidLoad ProfileUserPage');
  }

}
