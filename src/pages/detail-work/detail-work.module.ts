import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailWorkPage } from './detail-work';

@NgModule({
  declarations: [
    DetailWorkPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailWorkPage),
  ],
})
export class DetailWorkPageModule {}
