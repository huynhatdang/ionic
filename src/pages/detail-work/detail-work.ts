import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WorkAPI } from '../../shared/service/work.service';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the DetailWorkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-work',
  templateUrl: 'detail-work.html',
})
export class DetailWorkPage {

  work:any;
  address: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private _workAPI: WorkAPI, private toastCtrl: ToastController) {
    this.work = this.navParams.get('work');
    let arr = this.work.company.address.address.split(",");
    this.address = arr[arr.length - 2];
  }
  ionViewWillEnter() {
    let tabs = document.querySelectorAll('.tabbar');
    if ( tabs !== null ) {
      Object.keys(tabs).map((key) => {
        tabs[ key ].style.transform = 'translateY(56px)';
      });
    } // end if
  }

  ionViewDidLeave() {
    let tabs = document.querySelectorAll('.tabbar');
    if ( tabs !== null ) {
      Object.keys(tabs).map((key) => {
        tabs[ key ].style.transform = 'translateY(0)';
      });
    } // end if
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailWorkPage');
  }

  sendCV(){
    this._workAPI.sendCV(this.work._id).subscribe((data)=>{
      let toast = this.toastCtrl.create({
        message: 'Bạn đã gửi CV thành công!',
        duration: 1000,
        position: 'middle'
      });
    
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
    
      toast.present();
    })
  }

}
