import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Component, trigger, transition, style, animate, keyframes, ViewChild, ElementRef } from '@angular/core';

/**
 * Generated class for the TimViecPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tim-viec',
  templateUrl: 'tim-viec.html',
  animations: [
    trigger('flyInOut', [
      transition(':enter', [
        animate(3000, keyframes([
          style({opacity: 0, transform: 'translateX(-100%)', offset: 0}),
          style({opacity: 1, transform: 'translateX(15px)',  offset: 0.3}),
          style({opacity: 1, transform: 'translateX(0)',     offset: 1.0})
        ]))
      ]),
      transition(':leave', [
        animate(3000, keyframes([
          style({opacity: 1, transform: 'translateX(0)',     offset: 0}),
          style({opacity: 1, transform: 'translateX(-15px)', offset: 0.7}),
          style({opacity: 0, transform: 'translateX(100%)',  offset: 1.0})
        ]))
      ])
    ])
  ]
})
export class TimViecPage {

  @ViewChild('input') myInput: ElementRef
  searchtext: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TimViecPage');
  }

  search(){
    this.navCtrl.push('ResultworkPage',{'searchText':this.searchtext});
  }

}
