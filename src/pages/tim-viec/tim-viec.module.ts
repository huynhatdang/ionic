import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimViecPage } from './tim-viec';

@NgModule({
  declarations: [
    TimViecPage,
  ],
  imports: [
    IonicPageModule.forChild(TimViecPage),
  ],
})
export class TimViecPageModule {}
