import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkPostedPage } from './work-posted';

@NgModule({
  declarations: [
    WorkPostedPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkPostedPage),
  ],
})
export class WorkPostedPageModule {}
