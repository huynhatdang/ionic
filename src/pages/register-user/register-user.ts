import { role } from './../../shared/constant';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserAPI } from '../../shared/service/user.service';

/**
 * Generated class for the RegisterUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-user',
  templateUrl: 'register-user.html',
  providers: [FormBuilder]
})
export class RegisterUserPage {

  registerForm : FormGroup;
  submited: boolean = false;
  location: any ={
    latitude: null,
    longitude: null
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private _userAPI: UserAPI, private toastCtrl: ToastController, public modalCtrl: ModalController) {
    this.registerForm = this.formBuilder.group({
      fullname: ['', Validators.required],
      username: ['',Validators.compose([Validators.required,Validators.maxLength(15)])],
      email: ['',Validators.compose([Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)])],  
      address: ['', Validators.required],    
      password: ['',Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterCompanyPage');
  }
  addShake(type:string){
    document.querySelector(`.${type}`).classList.add("error","animated","shake");
    setTimeout(()=> {
    document.querySelector(`.${type}`).classList.remove("error","animated","shake");
    }, 1000);
  }

  register(){
    if( this.registerForm.valid){
      let user = {
        email: this.registerForm.get('email').value,
        username: this.registerForm.get('username').value,
        fullname: this.registerForm.get('fullname').value,
        address: {
          address: this.registerForm.get("address").value,
          location: this.location
        },
        password: this.registerForm.get('password').value,
        roles: role.USER
      }
      this._userAPI.createUser(user).subscribe((data)=>{
        let toast = this.toastCtrl.create({
					message: 'Bạn đã tạo tài khoản thành công!',
					duration: 1000,
					position: 'bottom'
				  });
				  toast.present();
				  this.navCtrl.push('LoginPage', {}, { animate: true, animation: 'ios-transition' })
      }, (err)=>{
        let toast = this.toastCtrl.create({
          message: 'Tạo tài khoản không thành công!',
          duration: 1000,
					position: 'bottom'
				  });
				  toast.present();
      })
    }
    let arr =['fullname','username','email','address','password'];
    arr.forEach((e)=>{
      if(this.registerForm.get(e).invalid){
        this.addShake(e);
      }
    })   
  }

  launchLocationPage() {
    let modal = this.modalCtrl.create('TestmapPage');

    modal.onDidDismiss((location) => {
      this.location.latitude = location.lat;
      this.location.longitude = location.lng;
      this.registerForm.controls['address'].setValue(location.name);
      console.log(location);
    });

    modal.present();

  }
}
