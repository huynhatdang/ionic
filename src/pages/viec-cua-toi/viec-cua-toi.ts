import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { WorkAPI } from './../../shared/service/work.service';
import { Component, trigger, transition, style, state, animate, keyframes } from '@angular/core';

/**
 * Generated class for the ViecCuaToiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viec-cua-toi',
  templateUrl: 'viec-cua-toi.html',
  animations: [
    trigger('flyInOut', [
      state('in', style({transform: 'translateX(0)'})),
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate(100)
      ]),
      transition(':leave', [
        animate(100, style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class ViecCuaToiPage {
  listFavorite: any[];

  constructor(private _workApi: WorkAPI, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
  }
  ionViewWillEnter(){
    let loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Getting Work...',
      duration: 100
    });
    loading.present().then(() => {
        this._workApi.getFavorite().subscribe((data) => {
          this.listFavorite = data.favorite.favorite;
        });
    })
  }
  
}
