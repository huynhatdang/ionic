import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViecCuaToiPage } from './viec-cua-toi';

@NgModule({
  declarations: [
    ViecCuaToiPage,
  ],
  imports: [
    IonicPageModule.forChild(ViecCuaToiPage),
  ],
})
export class ViecCuaToiPageModule {}
