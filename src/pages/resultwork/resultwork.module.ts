import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultworkPage } from './resultwork';

@NgModule({
  declarations: [
    ResultworkPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultworkPage),
  ],
})
export class ResultworkPageModule {}
