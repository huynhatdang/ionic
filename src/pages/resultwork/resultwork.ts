import { IWork } from './../../shared/interface/work';
import { WorkAPI } from './../../shared/service/work.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, FabContainer, ModalController } from 'ionic-angular';

/**
 * Generated class for the ResultworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resultwork',
  templateUrl: 'resultwork.html',
})
export class ResultworkPage {

  _listFilter: string;
  searchText: string;
  listFilterWork: any[];
  listWork: any[];
  filter: string = '';


  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter(value: string) {
    this._listFilter = value;
    this.listFilterWork = this.listFilter ? this.performFilter(this.listFilter) : this.listWork;
  }

  constructor(private _workApi: WorkAPI, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public modalCtrl: ModalController) {
    this.searchText = this.navParams.get('searchText');
  }


  ionViewDidLoad() {

    let loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Getting Work...'
    });
    loading.present().then(() => {
      this._workApi.searchWork(this.searchText, this.filter).subscribe((data) => {
        console.log(data);
        this.listWork = data.works;
        this.listFilterWork = this.listWork;
        console.log(this.listFilterWork);
        loading.dismiss();
      });
    })
  }

  filterBy() {
    let loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Getting Work...'
    });
    loading.present().then(() => {
      this._workApi.searchWork(this.searchText, this.filter).subscribe((data) => {
        console.log(data);
        this.listWork = data.works;
        this.listFilterWork = this.listWork;
        console.log(this.listFilterWork);
        loading.dismiss();
      });
    })
  }

  addfavorite(event, item) {
    if (item.favorite === false) {

    }
    this._workApi.addFavorite(item._id).subscribe((data) => {
      item.favorite =!item.favorite;
    })

    // this.listFilterWork.forEach((s) => {
    //   if(s.name == item.name && s.liked == false){
    //     s.liked = true;
    //   } else if( s.name == item.name && s.liked == true){
    //     s.liked = false;
    //   }
    // })
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  performFilter(filterBy: string): IWork[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.listWork.filter((work: IWork) =>
      work.name.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

  launchLocationPage() {
    let modal = this.modalCtrl.create('TestmapPage', { listWork: this.listFilterWork });

    modal.onDidDismiss((location) => {
      // this.location.latitude = location.lat;
      // this.location.longitude = location.lng;
      // this.registerForm.controls['address'].setValue(location.name);
      console.log(location);
    });

    modal.present();

  }

  viewDetail(work) {
    this.navCtrl.push('DetailWorkPage', { work: work }, { animate: true, animation: 'ios-transition' })
  }
}
