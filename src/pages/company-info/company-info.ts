import { CompanyAPI } from './../../shared/service/company.service';
import { IonicPage, NavController, ToastController, ModalController } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CompanyInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-company-info',
  templateUrl: 'company-info.html',
})
export class CompanyInfoPage {

  @ViewChild('signupSlider') signupSlider: any;

  companyForm: FormGroup;
  companyForm2: FormGroup;
  location: any ={
    latitude: null,
    longitude: null
  }

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, private toastCtrl: ToastController, private _companyAPI: CompanyAPI, private _storage: Storage, public modalCtrl: ModalController) {
    this.companyForm = formBuilder.group({
      name: ['', Validators.required],
      major: ['', Validators.required],
      contact: ['', Validators.compose([Validators.maxLength(12), Validators.minLength(10), Validators.pattern('[0-9]+'), Validators.required])],
      address: ['', Validators.required],
      dateTime: ['']
    });
    this._storage.get('user').then((data) => console.log(data));
    this._storage.get('token').then((data) => console.log(data));
    this.companyForm2 = formBuilder.group({
      human: ['', Validators.required],
      scale: ['', Validators.required],
      description: ['', Validators.required],
      website: ['', Validators.required],
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyInfoPage');
  }

  next() {
    this.signupSlider.slideNext();
  }

  prev() {
    this.signupSlider.slidePrev();
  }

  save() {
    console.log(this.companyForm2.get("description").value)
    let company = {
      name: this.companyForm.get("name").value,
      major: this.companyForm.get("major").value,
      contact: this.companyForm.get("contact").value,
      address: {
        address: this.companyForm.get("address").value,
        location: this.location
      },
      dateTime: this.companyForm.get("dateTime").value,
      human: this.companyForm2.get("human").value,
      scale: this.companyForm2.get("scale").value,
      description: this.companyForm2.get("description").value,
      website: this.companyForm2.get("website").value
    }
    this._companyAPI.createCompany(company).subscribe((data) => {
      console.log(data);
      let toast = this.toastCtrl.create({
        message: 'Thanh cong!',
        duration: 1000,
        position: 'bottom'
      });
      toast.present();
      this.navCtrl.push('CreateWorkPage');
    }, (err) => {
      // this.navCtrl.push('CreateWorkPage');
      console.log(err);
    });
    console.log(this.companyForm.value, this.companyForm2.value)
  }

  launchLocationPage() {
    let modal = this.modalCtrl.create('TestmapPage');

    modal.onDidDismiss((location) => {
      this.location.latitude = location.lat;
      this.location.longitude = location.lng;
      this.companyForm.controls['address'].setValue(location.name);
      console.log(location);
    });

    modal.present();

  }

}
