import { CompanyAPI } from './../shared/service/company.service';
import { WorkAPI } from './../shared/service/work.service';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SliderPage } from '../pages/slider/slider';
import { CvAPI } from '../shared/service/cv.service';
import { UserAPI } from '../shared/service/user.service';
import { ImagesProvider } from '../shared/service/images.service';
@Component({
  templateUrl: 'app.html',
  providers : [ WorkAPI, CvAPI, UserAPI, CompanyAPI, ImagesProvider ]
})

export class MyApp {
  rootPage:any = SliderPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

